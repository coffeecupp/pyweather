import nose
from weather.location import Location


class TestWeather():
    def __init__(self):
        self.location = Location('Vic', 'Melbourne')

    def test_instance(self):
        nose.tools.assert_is_instance(self.location, Location)

    def test_location_state(self):
        nose.tools.assert_equal(self.location.state.lower(), 'vic')

    def test_location_city(self):
        nose.tools.assert_equal(self.location.city.lower(), 'melbourne')
