import typing


class Location(object):
    def __init__(self, state: str = None,
                 city: str = None) -> None:
        self._state = state
        self._city = city

    @property
    def state(self) -> str:
        return self._state

    @state.setter
    def state(self, value: str) -> None:
        self.state = value

    @property
    def city(self) -> str:
        """ str: which city does the user want to look for. """
        return self._city

    @city.setter
    def city(self, value: str):
        self._city = value
